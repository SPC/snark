# Module snark-core



## Usage

## Artifact:

The Maven coordinates of this project are `space.kscience:snark-core:0.2.0-dev-1`.

**Gradle Kotlin DSL:**
```kotlin
repositories {
    maven("https://repo.kotlin.link")
    mavenCentral()
}

dependencies {
    implementation("space.kscience:snark-core:0.2.0-dev-1")
}
```
