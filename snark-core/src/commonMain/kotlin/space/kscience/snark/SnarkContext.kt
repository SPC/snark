package space.kscience.snark

/**
 * A marker interface for Snark Page and Site builders
 */
@SnarkBuilder
public interface SnarkContext