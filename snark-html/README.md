# Module snark-html



## Features

 - [data](#) : Data-based processing. Instead of traditional layout-based
 - [layouts](#) : Use custom layouts to represent a data tree
 - [parsers](#) : Add custom file formats and parsers using DataForge dependency injection
 - [preprocessor](#) : Preprocessing text files using templates
 - [metadata](#) : Trademark DataForge metadata layering and transformations
 - [dynamic](#) : Generating dynamic site using KTor server
 - [static](#) : Generating static site


## Usage

## Artifact:

The Maven coordinates of this project are `space.kscience:snark-html:0.2.0-dev-1`.

**Gradle Kotlin DSL:**
```kotlin
repositories {
    maven("https://repo.kotlin.link")
    mavenCentral()
}

dependencies {
    implementation("space.kscience:snark-html:0.2.0-dev-1")
}
```
